<?php

use Illuminate\Database\Seeder;

class LeaguesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leagues')->insert([
            'id' => uniqid(),
            'name' => 'Confician Alumni',
            'description' => 'Fantasy Draft League',
        //
    }
}
