@extends('master')

@section('custom-styles')
@endsection

@section('content')
    <div class="title text-center">
        @if (Auth::user()->joinAnyLeague())
            @foreach (Auth::user()->leagues as $league)
                 
            @endforeach
        @else
        <div class="row padding-top-lg">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default panel-primary">
                    <div class="panel-heading form-header">Join a League</div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{url('/join-league')}}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('league_code') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">League Code</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="league_code" value="{{ old('league_code') }}">
                                            @if ($errors->has('league_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('league_code') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('team_name') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Your Team Name</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="team_name">

                                            @if ($errors->has('team_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('team_name') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-success btn-block">
                                            <i class="fa fa-btn fa-sign-in"></i>Join Now!
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif
        
    </div>
@endsection

@section('custom-scripts')
@endsection

