<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    protected $table = 'user_activations';
    protected $primaryKey = 'user_id';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected function generateToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }

    public function setToken()
    {
        $token = $this->generateToken();
        $this->token = $token;
        $this->created_at = new \DateTime;
        return $token;
    }

    public static function getByToken($token)
    {
        return self::where('token', $token)->firstOrFail();
    }

}
