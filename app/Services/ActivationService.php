<?php

namespace App\Services;

use App\UserActivation;

class ActivationService
{
    protected $mailer;
    protected $resend_after = 24;

    public function sendActivationMail($user)
    {
        if ($user->activated || !$this->shouldSend($user))
        {
            return;
        }

        $token = $user->getActivationCode();

        $link = route('user.activate', $token);

        \Mail::send('auth.emails.activation', [ 'link' => $link ], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Account activation on MyBG.');
        });
    }

    public function activateUser($token)
    {
        try {
            $activation = UserActivation::getByToken($token);

            $user = $activation->user;
            $user->activated = true;
            $user->save();
            $activation->delete();
            return $user;

        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            return null;
        }
    }

    private function shouldSend($user)
    {
        $activation = $user->user_activation;
        return $activation === null || ((strtotime($activation->created_at) + 60 * 60 * $this->resend_after) < time());
    }

}

