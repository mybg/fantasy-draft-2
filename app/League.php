<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class League extends Authenticatable
{

    protected $fillable = [
        'id', 'name', 'description', 'created_at', 'updated_at'
    ];

    public function leagues()
    {
        return $this->belongsToMany('App\User');
    }
}
