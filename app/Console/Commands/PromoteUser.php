<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class PromoteUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:promote {username : username to proceed} {--demote : Whether to demote user to normal user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To promote user to admin, admin to superuser, or demote to normal user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $username = $this->argument('username');
            $demote = $this->option('demote');
            $user = User::getByUsername($username);
            $role = $user->role;
            $newRole = '';
            
            if (!$demote) {
                $this->promote($user, $role);
            }
            else {
                $this->demote($user, $role);
            }

            $this->info("Okay, operation stop.");
        }
        catch (exception $e)
        {
            $this->error('Unexpected errors caught, task terminated!');
            $this->error('Error messages: ' . $e->getMessage());
        }

    }

    private function promote($user, $role)
    {
        switch ($role) {
            case User::ROLE_USER:
                $newRole = User::ROLE_ADMIN;
                break;
            case User::ROLE_ADMIN:
                $newRole = User::ROLE_SUPERUSER;
                break;
            default:
                throw new Exception("User '$user->username' already with the superuser role.");
        }

        if ($this->confirm("Do you wish to promote '$user->username' to '$newRole'? [y|N]"))
        {
            $user->role = $newRole; 
            $user->save();
            $this->info("'$user->username' promoted to '$newRole'.");
            exit(1);
        }
        return false;
    }

    private function demote($user, $role)
    {
        if ($role == User::ROLE_USER) {
            $this->info("User '$user->username' already in normal user role.");
            return false;
        }
        if ($this->confirm("Do you wish to demote '$user->username' to normal user? [y|N]"))
        {
            $user->role = User::ROLE_USER;
            $user->save();
            $this->info("'$user->username' demoted to normal user.");
            exit(1);
        }
        return false;
    }
}
