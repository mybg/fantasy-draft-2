<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const ROLE_USER = 'user';
    const ROLE_SUPERUSER = 'root';
    const ROLE_ADMIN = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'role', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userActivation()
    {
        $this->hasOne('App\UserActivation');
    }

    public function leagues()
    {
        return $this->belongsToMany('App\League');
    }

    /**
     * Custom Functions 
     */
    public static function getByUsername($username)
    {
        return self::where('username', '=', $username)->firstOrFail();
    }

    public function getActivationCode()
    {
        $user_activation = $this->userActivation();
        if (is_null($user_activation))
        {
            $user_activation = new UserActivation();
            $user_activation->user_id = $this->id;
        }
        $token = $user_activation->setToken();
        $user_activation->save();

        return $token;
    }

    public function joinAnyLeague()
    {
        if ($this->leagues()->count() > 0)
        {
            return true;
        }

        return false;
    }

}
